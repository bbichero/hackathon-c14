# Ukeep

## History

The idea of Ukeep was born in July 2018 at the Scaleway Cold Storage hackathon   
in Paris.

Our goal was easy : transform an attractive but very technical and inaccessible   
service into an interface that could be used by ordinary people.

We won the 1st place and we decided to  make the project a reality and to    
solidify it.