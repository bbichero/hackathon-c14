const app 		= angular.module('C14_APP', ["ngRoute"]);
const ipc		= require('electron').ipcRenderer;
const remote	= require('electron').remote;
const clipboard	= require('electron').clipboard;
const getSize 		= require('get-folder-size');
const async			= require("async");
const pretty 		= require("prettysize");

app.config(function($routeProvider) {
    $routeProvider

        // home page
        .when('/', {
            templateUrl: 'home/index.html',
            controller: 'indexController'
        })

        // scheduledbackups list
        .when('/scheduled-backups/list', {
            templateUrl: 'scheduled-backups/list.html',
            controller: 'scheduledBackupController'
        })

        // scheduledbackups create
        .when('/scheduled-backups/create', {
            templateUrl: 'scheduled-backups/create.html',
            controller: 'scheduledBackupController'
        })

        // scheduledbackups uploads list
        .when('/scheduled-backups/:scheduledBackupId/uploads', {
            templateUrl: 'scheduled-backups/uploads.html',
            controller: 'scheduledBackupController'
        })

     	// scheduledbackups edits
        .when('/scheduled-backups/edit/:scheduledBackupId', {
            templateUrl: 'scheduled-backups/edit.html',
            controller: 'scheduledBackupController',
            resolve: {
		        ignored: function ($route) { $route.current.params.isEdit = true; }
		    }
        })

        // safes list
        .when('/safes/list', {
            templateUrl: 'safes/list.html',
            controller: 'safesController'
        })

        // safes create
        .when('/safes/create', {
            templateUrl: 'safes/create.html',
            controller: 'safesController'
        })

		.when('/safes/:safeId/archives', {
			templateUrl: 'archives/list.html',
			controller: 'archivesController'
		})

		.when('/settings', {
			templateUrl: 'home/settings.html',
			controller: 'settingsController'
		})

		.when("/login", {
			templateUrl: "home/login.html",
			controller: "loginController"
		})
});

// Angular JS
app.controller('indexController', ['$scope', '$rootScope', "$location", function ($scope, $rootScope, $location) {
	ipc.send('init');

	ipc.on("initDone", (event, arg) => {
		$rootScope.me = arg;
		$scope.init = true;
		$scope.$apply();
	});

	ipc.on("initFailed", (event, arg) => {
		Swal({
			toast: true,
			position: "top-end",
			timer: 2000,
			type: "error",
			title: "Initialization error",
			showConfirmButton: false
		});
	});

	ipc.on("notLogged", (event, arg) => {
		$rootScope.me = null;
		$location.path("/login");
		$scope.$apply();
	});
}]);

app.controller('loginController', ['$scope', '$rootScope', "$location", function ($scope, $rootScope, $location) {
	var loginPopup		= null;
	var verificationUrl	= null;
	$scope.userCode	= null;

	ipc.on("loginCode", (event, arg) => {
		$scope.userCode = arg.userCode;
		$scope.$apply();
		verificationUrl = arg.verificationUrl;
	});

	ipc.on("loginFailed", (event, arg) => {
		Swal({
			toast: true,
			position: "top-end",
			timer: 2000,
			type: "error",
			title: "Login failed",
			showConfirmButton: false
		});
		$scope.cancelLogin();
	});

	ipc.on("loginSuccess", (event, arg) => {
		Swal({
			toast: true,
			position: "top-end",
			timer: 2000,
			type: "success",
			title: "Logged in",
			showConfirmButton: false
		});
		$rootScope.me = arg;
		$scope.userCode = null;
		$scope.popupOpened = false;
		if (loginPopup) {
			loginPopup.close();
			loginPopup = null;
		}
		$location.path("/");
		$scope.$apply();
	});


	$scope.loginBtn = function () {
        ipc.send('loginBtn');
	};

	$scope.openPopup = function () {
		$scope.popupOpened = true;
		ipc.send("startLoginCheck");
		loginPopup = window.open(verificationUrl);
	};

	$scope.copyCode = function () {
		if (!$scope.userCode)
			return;

		clipboard.writeText($scope.userCode);
		Swal({
			toast: true,
			position: "top-end",
			timer: 2000,
			type: "success",
			title: 'Copied',
			showConfirmButton: false
		});
	};

	$scope.cancelLogin = function () {
		$scope.userCode = null;
		$scope.popupOpened = false;
		//$scope.$apply();
		if (loginPopup) {
			loginPopup.close();
			loginPopup = null;
		}
		ipc.send("cancelLogin");
	};
}]);

ipc.on('notification', (event, arg) => {
	new Notification(arg.title, {
		body: arg.message
	})
});

ipc.on('error', (event, arg) => {
	Swal('Errror!', arg, 'error');
	// remote.dialog.showErrorBox('Erreur !', arg);
});

ipc.on('success', (event, arg) => {
	Swal({
		toast: true,
		position: "top-end",
		timer: 2000,
		type: "success",
		title: arg,
		showConfirmButton: false
	});
	// Swal('Success!', arg, 'success');
	// remote.dialog.showErrorBox('Erreur !', arg);
});

ipc.on('uploadProgress', (event, arg) => {
	alert(arg);
});

ipc.on('uploadFinished', (event, arg) => {
	new Notification("Finished", {
		body: 'Successfully upload to C14 !'
	})
});

// Angular JS

app.controller("settingsController", ["$scope", "$rootScope", "$location", function ($scope, $rootScope, $location) {
	$scope.logout = function () {
		console.log("logout");
		ipc.send("logout");

		ipc.on("logoutSuccess", (event, arg) => {
			$rootScope.me = null;
			$location.path("/login");
			$scope.$apply();
		});
	};
}]);

app.controller('scheduledBackupController', ['$scope', '$rootScope', '$routeParams', '$location', function ($scope, $rootScope, $routeParams, $location) {
	$scope.create 				= false;
	$scope.scheduledBackupId	= $routeParams.scheduledBackupId;
	$scope.isEdit  				= !!$routeParams.isEdit;

  // Check the component destruction
  var destruction = false;

  if ($scope.scheduledBackupId && !$scope.isEdit) {
		function refresh() {
			setTimeout(function () {
				ipc.send('getScheduledBackupEvents', $scope.scheduledBackupId);
				refresh();
			}, 2000);
		}

		ipc.send('getScheduledBackupEvents', $scope.scheduledBackupId);

		ipc.on('getScheduledBackupEvents', (event, scheduledBackupsEvents) => {
			$scope.events = scheduledBackupsEvents;
			$scope.$apply();
		});

		$scope.runScheduledBackupNow = function (scheduledBackupId) {
			Swal({
				title: 'Confirm ?',
				text: 'This scheduled backup will be send to C14 now',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Yes',
				cancelButtonText: 'No'
			}).then((result) => {
			  	if (result.value) {
			    	Swal('Successfully', 'Running the scheduled backup process...', 'success');
	 				ipc.send('runScheduledBackupNow', scheduledBackupId);
			  	}
			});
		}

		refresh();
		return;
	}

	// Global funcs
	$scope.selectFolder = function () {
		remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
			properties: ['openDirectory']
		}, function (folderPath) {
			if (folderPath.length !== 1)
				return;

			$scope.scheduledBackup.folderPath = folderPath[0];
			$scope.$apply();
		});
	};

	if ($scope.scheduledBackupId && $scope.isEdit) {
		ipc.send('getScheduledBackupData', $scope.scheduledBackupId);

		ipc.on('getScheduledBackupData', (event, getScheduledBackupData) => {
			$scope.scheduledBackup = getScheduledBackupData;
			$scope.$apply();
		});

		$scope.updateScheduledBackup = function () {
			ipc.send('updateScheduledBackup', $scope.scheduledBackup);
			ipc.send('getScheduledBackup');

			$scope.scheduledBackup	= {}; // Empty the form
			$location.path("/scheduled-backups/list");
		};
		return;
	}

	// Request the scheduledBackup from the server
	ipc.send('getScheduledBackup');

	// Result from the server
	ipc.on('getScheduledBackup', (event, scheduledBackups) => {
		$scope.scheduledBackups = scheduledBackups;
		$scope.$apply();

    $scope.$on("$destroy", function() {
      destruction = true;
    });

    var cpt = 0;

    async.eachSeries($scope.scheduledBackups, function(file, callback) {
      if (destruction) {
        return callback();
      }

      getSize($scope.scheduledBackups[cpt].folderPath, (err, size) => {
        if (err) {
          $scope.scheduledBackups[cpt].size = 'folderMoved';
          $scope.$apply();
          cpt++;

          return callback();
        }

        $scope.scheduledBackups[cpt].size = pretty(size);
        $scope.$apply();

        cpt++;
        return callback();
      });
    }, function(err) {
        if( err ) {
          return ;
        }
    });
	});

	$scope.createScheduledBackup = function () {
		ipc.send('createScheduledBackup', $scope.scheduledBackup);
		ipc.send('getScheduledBackup');

		$scope.create 			= false;
		$scope.scheduledBackup	= {}; // Empty the form

		Swal({
			toast: true,
			position: "top-end",
			timer: 2000,
			type: "success",
			title: "Successfully created",
			showConfirmButton: false
		});

		$location.path("/scheduled-backups/list");
	};

	$scope.openScheduledBackupFolder = function (scheduledBackupId) {
		ipc.send('openScheduledBackupFolder', scheduledBackupId);
	};

	$scope.deleteScheduledBackup = function (scheduledBackupId) {
		Swal({
			title: 'Confirmer ?',
			text: 'Cette backup régulière n\'aura plus lieu.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Oui, supprimer',
			cancelButtonText: 'Non'
		}).then((result) => {
		  	if (result.value) {
		    	Swal('Confirmée', 'Cette backup régulière n\'aura plus lieu.', 'success');
 				ipc.send('deleteScheduledBackup', scheduledBackupId);
				ipc.send('getScheduledBackup');
		  	}
		});
	};
}]);

app.controller('safesController', ['$scope', '$rootScope', function ($scope, $rootScope) {
	$scope.create = false;
	$scope.loaded = false;

	// Request the scheduledBackup from the server
	ipc.send('getSafesList');

	// Result from the server
	ipc.on('getSafesList', (event, safesList) => {
		$scope.safes = safesList;
		$scope.loaded = true;
		$scope.$apply();
	});

	$scope.createSafe = function () {
		ipc.send('createSafe', $scope.safe);
		ipc.send('getSafesList');

		$scope.create 			= false;
		$scope.safe				= {}; // Empty the form
	};

	$scope.deleteScheduledBackup = function (scheduledBackupId) {
		Swal({
			title: 'Confirm ?',
			text: 'This scheduled backup will be stopped.',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
		  	if (result.value) {
		    	Swal('Success!', 'This scheduled backup has been deleted.', 'success')
 				ipc.send('deleteScheduledBackup', scheduledBackupId);
				ipc.send('getScheduledBackup');
		  	}
		})
	};
}]);

app.controller('archivesController', ['$scope', '$rootScope', '$routeParams', function ($scope, $rootScope, $routeParams) {
	$scope.safeId = $routeParams.safeId;
	$scope.loade	= false;

	if (!$scope.safeId) {
		alert('STOP !');
	}

	// Request the scheduledBackup from the server
	ipc.send('getArchivesList', $scope.safeId);

	// Result from the server
	ipc.on('getArchivesList', (event, result) => {
		$scope.totalSize	= result.totalSize;
		$scope.archives 	= result.archives;
		$scope.loaded		= true;
		$scope.$apply();
	});

	$scope.downloadArchive = function (bucketId) {
		Swal({
			title: 'Download location',
			text: 'Select the folder where the bucket will be transfered',
			type: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
		  	if (result.value) {
		    	remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
					properties: ['openDirectory']
				}, function (folderPath) {
					if (folderPath.length !== 1)
						return;

					ipc.send('downloadArchive', {
						safeId: $scope.safeId,
						bucketId: bucketId,
						folderPath: folderPath[0]
					});
					// $scope.$apply();
				});
		  	}
		})
	}

	$scope.lockArchive = function (bucketId) {
		Swal({
			title: 'Confirm',
			text: 'This bucket will be archived to C14',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
		  	if (result.value) {
		  		ipc.send('archiveBucket', {
					safeId: $scope.safeId,
					bucketId: bucketId
				});

				setTimeout(function () {
					ipc.send('getArchivesList', $scope.safeId);
				}, 4000);
		  	}
		})
	}

	$scope.unlockArchive = function (bucketId) {
		Swal({
			title: 'Confirm',
			text: 'This bucket will be un-archived from C14',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
		  	if (result.value) {
 				ipc.send('unArchiveBucket', {
					safeId: $scope.safeId,
					bucketId: bucketId
				});

				setTimeout(function () {
					ipc.send('getArchivesList', $scope.safeId);
				}, 4000);
		  	}
		})
	}

	$scope.deleteArchive = function (bucketId) {
		Swal({
			title: 'Confirm',
			text: 'This bucket will be *forever* deleted from C14',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes',
			cancelButtonText: 'No'
		}).then((result) => {
		  	if (result.value) {
 				ipc.send('deleteBucket', {
					safeId: $scope.safeId,
					bucketId: bucketId
				});
		  	}
		})
	}

}]);
