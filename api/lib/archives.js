// TODO check if safe is correct
const async		= require("async");
const fct		= require("./utils.js");
const config	= require("../../config.json");
const safe		= require("./safes");
const Client	= require("ssh2").Client;
const SCPclient	= require("scp2").Client
const fs		= require("fs");

module.exports = {
	// List archives
	list: (safeId, callback) => {
		if (!safeId)
			return callback("No safe ID provided.");

		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive?count=100", (err, result) => {
			if (err && err.error)
				return callback("Can't list archives: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Create archives
	create: (safeId, infos, callback) => {
		if (!safeId)
			return callback("No safe ID provided.");
		if (!infos.name)
			return callback("No archive name provided.");
		if (!infos.description)
			return callback("No archive description provided.");
		if (infos.parity && ["standard", "intensive", "enterprise"].indexOf(infos.parity) < 0)
			return callback("Invalid parity type provided.");
		if (infos.protocols && ["SSH", "FTP", "WEBDAV"].indexOf(infos.protocols[0]) < 0)
			return callback("Invalid protocol type provided.");
		if (infos.platforms && ["1", "2"].indexOf(infos.platforms[0]) < 0)
			return callback("Invalid platform type provided.");
		infos.crypto = "none";

		fct.POST(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive", infos, (err, result) => {
			if (err && err.error)
				return callback("Can't create archive: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Get info for specific archive id
	getInfo: (safeId, archiveId, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");

		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId, (err, result) => {
			if (err && err.error)
				return callback("Can't get info for archive ID (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Get list of platform list for concern archive
	locations: (safeId, archiveId, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");

		fct.GET(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId + "/location", (err, result) => {
			if (err && err.error)
				return callback("Can't get location list for archive ID (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Edit a particular archive
	edit: (safeId, archiveId, name, description, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");

		fct.PATCH(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId, {
			"name": name,
			"description": description
		}, (err, result) => {
			if (err && err.error)
				return callback("Can't edit info of archive ID (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},
	// Archive the archive
	archive: (safeId, archiveId, callback) => {
		if (!safeId)
			return callback("No safe id provided.");
		if (!archiveId)
			return callback("No archive id provided.");

		fct.POST(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId + "/archive", null, (err, result) => {
			if (err && err.error)
				return callback("Can't archive archive (" + archiveId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// unarchive the archive
	unarchive: (safeId, archiveId, infos, callback) => {
		if (!safeId)
			return callback("No safe ID provided.");
		if (!archiveId)
			return callback("No archive id provided.");
		if ([true, false].indexOf(infos.rearchive) < 0)
			return callback("invalid rearchive argument");
		if (infos.protocols && ["SSH", "FTP", "WEBDAV"].indexOf(infos.protocols[0]) < 0)
			return callback("Invalid protocol type provided.");

		fct.POST(config.url + "/api/v1/storage/c14/safe/" + safeId + "/archive/" + archiveId + "/unarchive", infos, (err, result) => {
			if (err && err.error)
				return callback("Can't unarchive archive: " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Delete a particular safe
	Delete: (safeId, callback) => {
		fct.DEL(config.url + "/api/v1/storage/c14/safe/" + safeId, (err, result) => {
			if (err && err.error)
				return callback("Can't delete safe ID (" + safeId + "): " + err.error);
			if (err)
				return callback(err);
			return callback(null, result);
		});
	},

	// Download recursively file with given dir path
	downloadRecur: (client, sftp, path, localPath, callback) => {
		if (!fs.existsSync(localPath + path))
			fs.mkdirSync(localPath + path);

		sftp.readdir("." + path, function(err, list) {
			if (err)
				return callback(err);

			async.eachSeries(list, function (file, nextFile) {
				//console.log(localPath + path + file.filename);
				if (file.longname[0] === "d") {
					if (!fs.existsSync(localPath + path + file.filename))
						fs.mkdirSync(localPath + path + file.filename);

					return module.exports.downloadRecur(client, sftp, path + file.filename + "/", localPath, nextFile);
					return nextFile(null);
				}

				// Download file
				client.download("."+ path + file.filename, localPath + path + file.filename, function (err) {
					if (err)
						return nextFile(err);
					return nextFile(null);
				});
			}, function (err) {
				if (err)
					return callback(err);
				client.close();
				return callback();
			});
		});
	},

	// Download archive
	download: (safeId, archiveId, localPath, callback) => {
		if (!safeId)
			return callback("No safe ID provided.");
		if (!archiveId)
			return callback("No archive id provided.");
		if (!localPath)
			return callback("No local path provided.");
		if (!fs.existsSync(localPath))
			return callback("Invalid local path provided.");

		let conn = new Client();

		async.waterfall([
			function (nextFunction) {
				module.exports.getInfo(safeId, archiveId, (err, result) => {
					if (err)
						return nextFunction(err);
					if (!result.bucket || (result && !result.bucket.credentials))
						return nextFunction("No bucket for this archive");
					return nextFunction(null, result.bucket.credentials);
				});
			},
			function (credentials, nextFunction) {
				let hostName = credentials[0].uri;
				let port = credentials[0].uri;
				let username = credentials[0].login;
				let password = credentials[0].password;

				try {
					hostName = hostName.match(/@(.*?):/g);
					hostName = hostName[0].replace(/[@:]/g, '');

					port = port.replace(/ssh:\/\//g, '');
					port = port.match(/:(.*)/g);
					port = port[0].replace(/[:]/g, '');
				}
				catch (error) {
					return callback(error);
				}
				const infos = {
					host: hostName,
					port: port,
					user: username,
					password: password
				};

				return nextFunction(null, infos);
			},
			function (infos, nextFunction) {
				conn.on("ready", function() {
					return nextFunction(null, infos);
				}).connect(infos);
			},
			function (infos, nextFunction) {
				conn.sftp(function(err, sftp) {
					if (err) throw err;
					return nextFunction(null, infos, sftp);
				});
			},
			function (infos, sftp, nextFunction) {
				// connect to ssh archive
				let client = new SCPclient(infos);

				module.exports.downloadRecur(client, sftp, "/", localPath, function (err) {
					if (err)
						return nextFunction(err);
					return nextFunction();
				});
			},
		], function (err) {
			if (err)
				return callback(err);
			return callback(null, true);
		});
	}
};
