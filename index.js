const electron 		= require("electron");
const {shell, Menu} 		= require("electron");
const fs 			= require("fs");
const async			= require("async");
const client		= require("scp2");
const SftpUpload	= require("sftp-upload");
const colors		= require("colors/safe");
const Store			= require("electron-store");
const uuidv4 		= require("uuid/v4");
const pretty 		= require("prettysize");
const os 			= require("os");
const request		= require("request");
const moment		= require("moment");
const getSize 		= require("get-folder-size");

// ONLINE API
const safe			= require("./api/lib/safes");
const archive		= require("./api/lib/archives");
const online		= require("./api/lib/online");

// GLOBAL CONFIG
const config		= require("./config.json")

const store			= new Store({ // /Users/alex/Library/Application Support/app
	name: config.storeName,
	encryptionKey: config.storeEncryptionKey // Config file is crypt ;-)
});

// ELECTRON
const app 			= electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipc 			= electron.ipcMain;
const Tray 			= electron.Tray;

let defaultWindow;
let tryDelay		= null;
let loginInterval	= null;
let deviceCode		= null;
let tryNb			= 0;

app.setLoginItemSettings({
	openAtLogin: true,
	openAsHidden: true
});

if (!store.has('scheduledBackups')) {
	store.set('scheduledBackups', []);
	console.log(colors.cyan('[C14]'), "scheduledBackups array created in config file.");
}

checkBackupInterval();

/*************************************************************
						Functions
/*************************************************************/

function initWindow () {
	var template = [{
	    label: "Application",
	    submenu: [
	        { label: "About Application", selector: "orderFrontStandardAboutPanel:" },
	        { type: "separator" },
	        { label: "Quit", accelerator: "Command+Q", click: function() { app.quit(); }}
	    ]}, {
	    label: "Edit",
	    submenu: [
	        { label: "Undo", accelerator: "CmdOrCtrl+Z", selector: "undo:" },
	        { label: "Redo", accelerator: "Shift+CmdOrCtrl+Z", selector: "redo:" },
	        { type: "separator" },
	        { label: "Cut", accelerator: "CmdOrCtrl+X", selector: "cut:" },
	        { label: "Copy", accelerator: "CmdOrCtrl+C", selector: "copy:" },
	        { label: "Paste", accelerator: "CmdOrCtrl+V", selector: "paste:" },
	        { label: "Select All", accelerator: "CmdOrCtrl+A", selector: "selectAll:" }
	    ]}
	];
	
	Menu.setApplicationMenu(Menu.buildFromTemplate(template));
console.log("icon:", __dirname + "/build/icon.icns");

	defaultWindow = new BrowserWindow({
		width: 1000,
		height: 800,
		title: 'C14 - Desktop',
		minWidth: 700,
		minHeight: 500,
		icon: __dirname + "/build/icon.icns"
	});

	defaultWindow.loadURL("file://" + __dirname + "/assets/views/index.html");

	defaultWindow.on('closed', () => {
		defaultWindow = null;
	});

	console.log(colors.cyan('[C14]'), "Window created");
}

/*************************************************************
						BACKUPS
/*************************************************************/

function runBackup(scheduledBackupId) {
	let scheduledBackups 	= store.get('scheduledBackups');
	let scheduledBackup 	= null;

	for (var i = 0; i < scheduledBackups.length; i++) {
		if (scheduledBackups[i].id === scheduledBackupId) {
			scheduledBackup = scheduledBackups[i];
			break;
		}
	}

	if (!scheduledBackup) {
		console.log(colors.cyan('[C14]'), "Unable to found this scheduledBackup...", scheduledBackupId);
		return;
	}

	async.waterfall([
		function (callback) { // If no safe for the scheduledBackup, create new one.
			if (scheduledBackup.safeId)
				return callback(null, scheduledBackup.safeId); // If safe already created, skip.

			safe.create("[C14-DESKTOP] - " + scheduledBackup.prefix, "Created automatically by " + os.hostname(), (err, safeId) => {
				if (err)
					return callback("Unable to create safe: " + err);

				for (var i = 0; i < scheduledBackups.length; i++) {
					if (scheduledBackups[i].id === scheduledBackupId) {
						scheduledBackups[i].safeId = safeId;
						break;
					}
				}

				scheduledBackup.safeId	= safeId;
				store.set('scheduledBackups', scheduledBackups); // Update in DB.

				console.log(colors.cyan('[C14]'), "Create safe:", colors.green(safeId), "for scheduledBackup", colors.green(scheduledBackup.id));
				return callback(null, safeId);
			});
		},
		function (safeId, callback) { // Create archive for the new backup cycle
			let backupInProgress = scheduledBackup.uploads.find(function(element) {
			  	return (element.status === "init");
			});

			if (backupInProgress) // Retry to upload to this archive.
				return callback(null, safeId, backupInProgress.archiveId, backupInProgress.id);

			let randomId = uuidv4();

			archive.create(scheduledBackup.safeId, {
				name: "[AUTO] - " + randomId.substring(0, 8),
				description: "[AUTO] by " + os.hostname(),
				parity: scheduledBackup.type,
				protocols: ["SSH"],
				platforms: ["1"]
			}, (err, archiveId) => {
				if (err)
					return callback(err);

				let uploadId = uuidv4();

				scheduledBackup.uploads.push({
					id: uploadId,
					status: "init",
					archiveId: archiveId,
					startDate: Math.round(Date.now() / 1000),
					endDate: null,
					percent: 0,
					size: 0
				});

				store.set('scheduledBackups', scheduledBackups); // Update in DB.

				console.log(colors.cyan('[C14]'), "Create archive:", colors.green(archiveId), "for scheduledBackup", colors.green(scheduledBackup.id));
				
				setTimeout(function () {
					return callback(null, safeId, archiveId, uploadId);
				}, 10000);
			});
		},
		function (safeId, archiveId, uploadId, callback) { // Uploads
			console.log(colors.cyan('[C14]'), "Find archive credentials for", colors.green(scheduledBackup.id));

			archive.getInfo(safeId, archiveId, (err, archiveDetails) => {
				if (err)
					return callback(err);

				if (!fs.existsSync(scheduledBackup.folderPath)) {
					console.log(scheduledBackup.uploads[scheduledBackup.uploads.length]);
					scheduledBackup.uploads[scheduledBackup.uploads.length].status = "error";
					scheduledBackup.uploads[scheduledBackup.uploads.length].message = "Folder not found.";
					console.log(colors.cyan('[C14]'), "Folder not found !!");
				}

				if (archiveDetails.status === "busy") {
					// The bucket is not ready.
					// wait 5 seconds and re-run the upload function.

					console.log(colors.cyan('[C14]'), "Archive is not ready, retrying in 30 seconds...");

					setTimeout(function () {
						runBackup(scheduledBackupId);
					}, 30000);

					return;
				}

				let folderSize		= fs.statSync(scheduledBackup.folderPath).size,
					hostName		= archiveDetails.bucket.credentials[0].uri,
					port 			= archiveDetails.bucket.credentials[0].uri;

				try {
					hostName 		= hostName.match(/@(.*?):/g);
					hostName 		= hostName[0].replace(/[@:]/g, '');

					port 			= port.replace(/ssh:\/\//g, '');
					port 			= port.match(/:(.*)/g);
					port 			= port[0].replace(/[:]/g, '');
				}
				catch (error) {
				 	return callback(error);
				}

				let uploadConfig	= {
					host: hostName,
					port: port,
					username: archiveDetails.bucket.credentials[0].login,
					password: archiveDetails.bucket.credentials[0].password,
					path: scheduledBackup.folderPath,
					remoteDir: "uploads/"
				};

				console.log(colors.cyan('[C14]'), "Start upload of", colors.green(scheduledBackup.folderPath), colors.green(pretty(folderSize)));

				sftp = new SftpUpload(uploadConfig);

				sftp.on('error', function (err) {
					if (err.code === "ENOTFOUND") {
						console.log(colors.cyan('[C14]'), "Host (" + hostName + ") is not ready, retrying in 15 seconds...");

						setTimeout(function () {
							runBackup(scheduledBackupId);
						}, 15000);
						return;
					}

					console.log(colors.cyan('[C14]'), "Upload error:", colors.red(err));

					// Update error in uploads array
					let i = scheduledBackup.uploads.findIndex((x => x.id === uploadId));

					scheduledBackup.uploads[i].status 	= "failed";
					scheduledBackup.uploads[i].message	= "Error -> " + err;

					store.set('scheduledBackups', scheduledBackups);
					return callback(err);
				})
				.on('uploading', function (progress) {
					// Update percent in uploads array

					let i = scheduledBackup.uploads.findIndex((x => x.id === uploadId));

					scheduledBackup.uploads[i].percent = progress.percent;
					scheduledBackup.uploads[i].message = "progress";

					if (!scheduledBackup.uploads[i].size)
						scheduledBackup.uploads[i].size	= folderSize;

					store.set('scheduledBackups', scheduledBackups);

					// Debug
					console.log(colors.cyan('[C14]'), "Uploading", colors.green(progress.file));
					console.log(colors.cyan('[C14]'), "Uploading", colors.green(progress.percent) + '% completed');
				})
				.on('completed', function() {
					// Update percent in uploads array

					let i = scheduledBackup.uploads.findIndex((x => x.id === uploadId));

					scheduledBackup.uploads[i].status 	= "finished";
					scheduledBackup.uploads[i].endDate 	= Math.round(Date.now() / 1000);
					scheduledBackup.lastBackupDate		= Math.round(Date.now() / 1000)

					store.set('scheduledBackups', scheduledBackups);
				    return callback(null, safeId, archiveId, uploadId);
				})
				.upload();
			});
		},
		function (safeId, archiveId, uploadId, callback) { // Close the archive on C14
			archive.archive(safeId, archiveId, (err, result) => {
				if (err)
					return callback(err);

				console.log(colors.cyan('[C14]'), "Close successfully the bucket", colors.green(archiveId));
				return callback();
			});
		}
	], function (err, r) {
		if (err)
			console.log(colors.cyan('[C14]'), "Error initSheduledBackup ->", err);
	});
}

function checkBackupInterval () {
	console.log(colors.cyan('[C14]'), "Run check interval");

	let scheduledBackups 	= store.get('scheduledBackups');
	let currentTime 		= Math.round(Date.now() / 1000);

	// Deal with scheduled backups
	scheduledBackups.map(x => {
		if (x.isPaused) // Skip
			return;

		// Check if a new bakcup is required
		if ((currentTime - x.lastBackupDate) > x.interval) {
			// Triggers that we can ask for a backup
			console.log(colors.cyan('[C14]'), "Scheduled backup", x.prefix, "start");
			// console.log(colors.cyan('[C14]'), "Debug", colors.green(`SUCCESS: ${currentTime - x.lastBackupDate} > ${x.interval}`));
			runBackup(x.id);
		} else {
			console.log(colors.cyan('[C14]'), "Scheduled backup", x.prefix, "still waiting");
			// console.log(colors.cyan('[C14]'), "Debug", colors.red(`ERR: ${currentTime - x.lastBackupDate} < ${x.interval}`));
		}
	});

	setTimeout(function () {
		checkBackupInterval();
	}, (1000 * 60 * 5))
}

/*************************************************************
						Windows Events
/*************************************************************/

app.on('ready', initWindow);

app.on('window-all-closed', () => {
 	if (process.platform !== 'darwin') {
 		console.log(colors.cyan('[C14]'), "Close the app");
		app.quit();
	}
});

app.on('activate', () => {
	if (defaultWindow === null) {
		initWindow();
	}
});

/*************************************************************
						EVENTS
/*************************************************************/

ipc.on("init", (event, folders) => {
	if (!isLogged())
		return event.sender.send("notLogged");

	online.userInfo((err, info) => {
		if (err)
			return event.sender.send("initFailed");

		return event.sender.send("initDone", info);
	});
	//event.sender.send('getParams', params);
});

/*************************************************************
						COFFRES (SAFE)
/*************************************************************/

ipc.on('getSafesList', (event) => {
	// TODO -> Check if logged !

	safe.list((err, safeList) => {
		if (err)
			return event.sender.send('error', 'Unable to get safe: ' + err);

		for (var i = 0; i < safeList.length; i++) {
			if (safeList[i].name.includes("[C14-DESKTOP]")) {
				safeList[i].name = safeList[i].name.replace("[C14-DESKTOP] - ", "");
				safeList[i].isScheduledBackup = true;
			}
		}

		async.eachOfLimit(safeList, 3, (safe, key, cb) => {
			archive.list(safe.uuid_ref, (err, list) => {
				if (err)
					return cb(err);

				safeList[key].bucketCount = list.length;
				return cb(null);
			});
		},
		(err) => {
			if (err)
				return console.error(err);

			event.sender.send('getSafesList', safeList);
		});

	});
});

ipc.on('createSafe', (event, safeData) => {
	// TODO -> Check if logged !

	if (!safeData || !safeData.name || !safeData.description)
		return event.sender.send('error', 'Il manque des champs !');

	safe.create(safeData.name, safeData.description, (err, safeId) => {
		if (err)
			return event.sender.send('error', 'Unable to create safe: ' + err);

		console.log(colors.cyan('[C14]'), "New safe created with id:", colors.green(safeId));
		return event.sender.send('notification', 'Successfully created !');
	});
});

/*************************************************************
						BUCKETS
/*************************************************************/

ipc.on('getArchivesList', (event, safeId) => {
	// TODO -> Check if logged !

	console.log(colors.cyan('[C14]'), "Load safe with ID ->", colors.green(safeId));

	archive.list(safeId, (err, archivesList) => {
		if (err)
			return event.sender.send('error', 'Unable to get archives: ' + err);

		let totalSize = 0;

		async.eachSeries(archivesList, function (archiveData, nextArchive) {
			archive.getInfo(safeId, archiveData.uuid_ref, (err, archiveDetails) => {
				if (err)
					return nextArchive(err);

				archiveData.type = archiveDetails.parity;

				if (!archiveDetails.bucket) {
					archiveData.statusName	= "closed";

					if (archiveDetails.size !== undefined)
						totalSize 			+= Number(archiveDetails.size);

					archiveData.size 	= pretty(archiveDetails.size);
				}
				else {
					archiveData.statusName	= "open";
					archiveData.size 	= "N/A";
				}

				if (archiveData.status === "busy")
					archiveData.statusName	= "busy";

				return nextArchive();
			});
		}, function (err) {
			if (err)
				return event.sender.send('error', 'Unable to get archives: ' + err);

			event.sender.send('getArchivesList', {
				totalSize: pretty(totalSize),
				archives: archivesList
			});
		});
	});
});

ipc.on('unarchiveArchive', (event, safeId) => {
	// TODO -> Check if logged !
});

ipc.on('archiveBucket', (event, data) => {
	// TODO -> Check if logged !

	if (!data || !data.safeId || !data.bucketId)
		return event.sender.send('error', 'Il manque des champs !');

	archive.getInfo(data.safeId, data.bucketId, (err, archiveDetails) => {
		if (err)
			return event.sender.send('error', err);

		archive.archive(data.safeId, data.bucketId, (err, result) => {
			if (err)
				return event.sender.send('error', err);

			console.log(colors.cyan('[C14]'), "Archive successfully the bucket", colors.green(data.bucketId));
			return event.sender.send('success', 'Successfully archived !');
		});
	});
});

ipc.on('unArchiveBucket', (event, data) => {
	// TODO -> Check if logged !

	if (!data || !data.safeId || !data.bucketId)
		return event.sender.send('error', 'Il manque des champs !');

	archive.getInfo(data.safeId, data.bucketId, (err, archiveDetails) => {
		if (err)
			return event.sender.send('error', err);

		archive.locations(data.safeId, data.bucketId, (err, locations) => {
			if (err)
				return event.sender.send('error', err);

			archive.unarchive(data.safeId, data.bucketId, {
				location_id: locations[0].uuid_ref,
				rearchive: false,
				protocols: ["SSH"]
			}, (err, result) => {
				if (err)
					return event.sender.send('error', err);

				console.log(colors.cyan('[C14]'), "Un-archive successfully the bucket", colors.green(data.bucketId));
				return event.sender.send('success', 'Successfully un-archived !');
			});
		});
	});
});

ipc.on('downloadArchive', (event, data) => {
	// TODO -> Check if logged !

	if (!data || !data.safeId || !data.bucketId || !data.folderPath)
		return event.sender.send('error', 'Il manque des champs !');

	archive.download(data.safeId, data.bucketId, data.folderPath, (err, result) => {
		if (err)
			return event.sender.send('error', err);

		console.log(colors.cyan('[C14]'), "Download successfully the bucket", colors.green(data.bucketId));
		
		return event.sender.send('notification', 'Download finished !');
	});
});

ipc.on('deleteArchive', (event, data) => {
	// TODO -> Check if logged !

	if (!data || !data.safeId)
		return event.sender.send('error', 'Il manque des champs !');

	archive.Delete(data.safeId, (err, result) => {
		if (err)
			return event.sender.send('error', err);

		console.log(colors.cyan('[C14]'), "Delete successfully the bucket", colors.green(data.bucketId));
		return event.sender.send('success', 'Successfully deleted !');
	});
});


/*************************************************************
					Scheduled Backups
/*************************************************************/

ipc.on('getScheduledBackup', (event, scheduledBackup) => {
	let scheduledBackups = store.get('scheduledBackups');

	for (var i = 0; i < scheduledBackups.length; i++) {
		if (scheduledBackups[i].safeId) {
			scheduledBackups[i].status = {
				message: "OK",
				class: "green"
			}
		}

		if (!scheduledBackups[i].safeId) {
			scheduledBackups[i].status = {
				message: "Waiting creation",
				class: "orange"
			}
		}

		if (scheduledBackups[i].isPaused) {
			scheduledBackups[i].status = {
				message: "Paused",
				class: "red"
			}
		}

		if (scheduledBackups[i].lastBackupDate)
			scheduledBackups[i].lastBackupDate = moment.unix(scheduledBackups[i].lastBackupDate).fromNow();
		else
			scheduledBackups[i].lastBackupDate = null;
	}

	event.sender.send('getScheduledBackup', scheduledBackups);
});

ipc.on('getScheduledBackupData', (event, scheduledBackupId) => {
	let scheduledBackups = store.get('scheduledBackups');

	let i = scheduledBackups.findIndex((x) => {
		return x.id === scheduledBackupId;
	});

	if (i === -1)
		return; // Not found

	event.sender.send('getScheduledBackupData', scheduledBackups[i]);
});


ipc.on('getScheduledBackupEvents', (event, scheduledBackupId) => {
	let scheduledBackups = store.get('scheduledBackups');

	let i = scheduledBackups.findIndex((x) => {
		return x.id === scheduledBackupId;
	});

	if (i === -1)
		return; // Not found

	for (var x = 0; x < scheduledBackups[i].uploads.length; x++) {
		scheduledBackups[i].uploads[x].size = pretty(scheduledBackups[i].uploads[x].size);

		if (scheduledBackups[i].uploads[x].startDate)
			scheduledBackups[i].uploads[x].startDate	= moment.unix(scheduledBackups[i].uploads[x].startDate).fromNow();
		else
			scheduledBackups[i].uploads[x].startDate	= null;

		if (scheduledBackups[i].uploads[x].endDate)
			scheduledBackups[i].uploads[x].endDate	= moment.unix(scheduledBackups[i].uploads[x].endDate).fromNow();
		else
			scheduledBackups[i].uploads[x].endDate	= null;

		if (scheduledBackups[i].uploads[x].status === "finished") {
			scheduledBackups[i].uploads[x].status = {
				message: "Finished",
				class: "green"
			}
		}

		if (scheduledBackups[i].uploads[x].status === "progress" || scheduledBackups[i].uploads[x].status === "init") {
			scheduledBackups[i].uploads[x].status = {
				message: "Progress...",
				class: "orange"
			}
		}

		if (scheduledBackups[i].uploads[x].status === "failed") {
			scheduledBackups[i].uploads[x].status = {
				message: "Failed",
				class: "red"
			}
		}
	}

	event.sender.send('getScheduledBackupEvents', scheduledBackups[i].uploads);
});

ipc.on('createScheduledBackup', (event, scheduledBackup) => {
	if (!scheduledBackup || !scheduledBackup.prefix || !scheduledBackup.type || !scheduledBackup.interval || !scheduledBackup.folderPath)
		return event.sender.send('error', 'Il manque des champs !');

	let scheduledBackups 			= store.get('scheduledBackups');

	scheduledBackup.id				= uuidv4();
	scheduledBackup.lastBackupDate 	= 0;
	scheduledBackup.uploads			= [];
	scheduledBackup.isPaused 		= false;

	scheduledBackups.push(scheduledBackup);
	store.set('scheduledBackups', scheduledBackups);

	// Run the first backup.
	runBackup(scheduledBackup.id);
});

ipc.on('updateScheduledBackup', (event, scheduledBackup) => {
	if (!scheduledBackup || !scheduledBackup.id || !scheduledBackup.prefix || !scheduledBackup.type || !scheduledBackup.interval || !scheduledBackup.folderPath)
		return event.sender.send('error', 'Il manque des champs !');

	let scheduledBackups = store.get('scheduledBackups');

	let i = scheduledBackups.findIndex((x) => {
		return x.id === scheduledBackup.id;
	});

	if (i === -1)
		return; // Not found

	scheduledBackups[i].prefix		= scheduledBackup.prefix;
	scheduledBackups[i].type 		= scheduledBackup.type;
	scheduledBackups[i].interval	= scheduledBackup.interval;
	scheduledBackups[i].folderPath 	= scheduledBackup.folderPath;
	scheduledBackups[i].isPaused 	= scheduledBackup.isPaused;

	store.set('scheduledBackups', scheduledBackups);
	return event.sender.send('success', 'Successfully updated !');
});

ipc.on('runScheduledBackupNow', (event, scheduledBackupId) => {
	if (!scheduledBackupId)
		return event.sender.send('error', "Invalid backupId !");

	let scheduledBackups 	= store.get('scheduledBackups');

	let i = scheduledBackups.findIndex((x) => {
		return x.id === scheduledBackupId;
	});

	if (i === -1)
		return event.sender.send('error', "Backup not found...");

	runBackup(scheduledBackupId);
});

ipc.on('openScheduledBackupFolder', (event, scheduledBackupId) => {
	let scheduledBackups 	= store.get('scheduledBackups');
	let isFound 			= false;

	for (var i = 0; i < scheduledBackups.length; i++) {
		if (scheduledBackups[i].id === scheduledBackupId) {
			shell.showItemInFolder(scheduledBackups[i].folderPath);
			isFound = true;
			break;
		}
	}

	if (!isFound)
		return event.sender.send('error', 'Impossible de trouver ce dossier !');
});

ipc.on('deleteScheduledBackup', (event, scheduledBackupId) => {
	let scheduledBackups 	= store.get('scheduledBackups');
	let isDeleted 			= false;

	for (var i = 0; i < scheduledBackups.length; i++) {
		if (scheduledBackups[i].id === scheduledBackupId) {
			scheduledBackups.splice(i, 1);
			isDeleted = true;
			break;
		}
	}

	if (!isDeleted)
		return event.sender.send('error', 'Impossible de supprimer cette backup !');

	store.set('scheduledBackups', scheduledBackups);
});

/*************************************************************
							LOGIN
/*************************************************************/

function isLogged() {
	let auth = store.get("auth");

	if (!auth || !auth.expiration || !auth.tokenType || !auth.accessToken || moment(auth.expiration).isBefore(moment()))
		return false;

	return true;
}

function clearLogin() {
	tryDelay		= null;
	loginInterval	= null;
	deviceCode		= null;
	tryNb			= 0;
}

function tryLogin(event) {
	if (tryNb === 20) {
		clearInterval(loginInterval);
		clearLogin();
		event.sender.send("loginFailed");
		return;
	}
	++tryNb;

	let formData	= {
		client_id: config.clientId,
		client_secret: config.clientSecret,
		code: deviceCode,
		grant_type: "http://oauth.net/grant_type/device/1.0"
	};

	request({
		uri: "https://console.online.net/oauth/v2/token",
		method: "POST",
		formData: formData,
		json: true
	}, (error, response, body) => {
		if (error) {
			clearInterval(loginInterval);
			clearLogin();
			event.sender.send("loginFailed");
			return console.error("Login failed", error);
		}

		if (response.statusCode === 400 || body.error)
			return;

		if (response.statusCode === 200) {
			clearInterval(loginInterval);
			clearLogin();

			if (!body.access_token || !body.token_type || !body.expires_in)
				return event.sender.send("loginFailed");

			console.log(colors.cyan('[C14]'), "Logged, token =", body.access_token);
			let authInfo = {
				tokenType: body.token_type,
				accessToken: body.access_token,
				expiration: moment().add(body.expires_in, "s").format()
			};
			store.set("auth", authInfo);
			online.userInfo((err, infos) => {
				if (err) {
					store.set("auth", null);
					return event.sender.send("loginFailed");
				}

				return event.sender.send("loginSuccess", infos);
			});
		}
	});
}

ipc.on("loginBtn", (event, arg) => {
	if (!config.clientId)
		return event.sender.send("loginFailed");

	request({
		uri: "https://console.online.net/oauth/v2/device/code?client_id=" + config.clientId,
		method: "POST",
		json: true
	}, (error, response, body) => {
		if (error)
			return event.sender.send("loginFailed");

		if (!body.interval || !body.device_code || !body.user_code || !body.verification_url) {
			clearLogin();
			return event.sender.send("loginFailed");
		}

		tryDelay = body.interval;
		deviceCode = body.device_code;
		event.sender.send("loginCode", {userCode: body.user_code, verificationUrl: body.verification_url});
	});
	return;
});

ipc.on("startLoginCheck", (event, arg) => {
	if (!tryDelay)
		return console.error("No login interval");

	loginInterval = setInterval(() => {tryLogin(event);}, tryDelay * 1000);
	return;
});

ipc.on("cancelLogin", (event, arg) => {
	if (!loginInterval)
		return;

	clearInterval(loginInterval);
	return;
});

ipc.on("logout", (event, arg) => {
	store.set("auth", null);

	event.sender.send("logoutSuccess");
});
