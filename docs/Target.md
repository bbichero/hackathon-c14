Targets
------

The application will serve different needs :

- On a personal computer with a standard user, the application will be used    
to perform basic backup (songs, movies, personal files, ...).    

- After downloading the Application on a desktop or a mobile, user will be    
able to choose, when and what to backup with a friendly dashboard.   

- On the other hand the application will be very customizable, each features    
can be easily disable or enable with a simple click and each users will have    
its own customized application.

- Companies will be able to create "slaves" accounts managed by "masters"    
account that configures which folders needs to be backup.    

- An user will have the possbility to choose where its data is stored, on a   
public providers or on his own infrastructure. 