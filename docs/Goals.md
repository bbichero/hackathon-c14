Goals
-------

The main idea is to provide a simple way for backuping.   
But many ways exist for backuping: Cold, Fast, Persistent, Decentralized, ...
The goals is to provide all of thoses in one interface.   

API must be hostable easily on multiples devices.
From the computer who run the front to a remote server, API will provides routes to communicate directly with providers.   
Following your backup setting, the API will choose how to backup your data.

The application is light, a minimum of dependencies have been installed to consume a minimum of RAM. 
Front application can adapt the display following your level of settings you want.   
Features will be plugable easly to suite your needs.
