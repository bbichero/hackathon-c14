# Ukeep documentations
------

### What ?
Ukeep is a desktop application that makes managing your backups easier.

### Why ?
It figures out that today, backup solutions are not so easy and fast to use or host as we would like.   
A desktop application connected to any sort of backup backend will save you some time.

### Where ?
Everywhere, you can find the desktop application on each systems, as for the mobile app.   
The Ukeep API can be hosted can be hosted at home or on a remote server.


### For who ?
Everyone : startup, big business, family, anyone who needs persistent backups.

- [Validation process](Validation_process.md)
- [Goals](Goals.md)
- [Target](Target.md)
- [Feature](Feature.md)

Roundmap:
------

- [First month](Roundmap/month_1.md)
  * Choose the most relevant langage / libraries for development steps
  * POC of API, Front and mobile APP
  * Unit test for front and API

- [Second month](Roundmap/month_2.md)
  * Front: user settings + homepage
  * API: route for user setting + user informations
  * Unit test for front and API

- [Third month](Roundmap/month_3.md)
  * Front: user dashboard + c14 storage implementation
  * API: route for c14 storage + admin account / customization
  * Unit test for front and API

- [Fourth month](Roundmap/month_4.md)
  * Start mobile APP: user setting, homepage, user dashboard
  * Unit test for front and API
  * Continuous integration, gitlab CI ...

- [Fifth month](Roundmap/month_5.md)
  * S3 integration with scaleway + local S3
  * Local storage for cold backup (find techno)
  * Unit test for front and API
